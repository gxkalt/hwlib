
SOCEDS_ROOT ?= $(SOCEDS_DEST_ROOT)
HWLIBS_ROOT = $(CYC5SOC_HWLIB_SRC_PATH)

CROSS_COMPILE := arm-altera-eabi-
CC := $(CROSS_COMPILE)gcc
LD := $(CROSS_COMPILE)g++
NM := $(CROSS_COMPILE)nm
AR := $(CROSS_COMPILE)ar
OD := $(CROSS_COMPILE)objdump
OC := $(CROSS_COMPILE)objcopy
RANLIB := $(CROSS_COMPILE)ranlib
RM := rm -rf
CP := cp -f

PROCESSOR = -mcpu=cortex-a9
CFLAGS += -g -O0 -mfloat-abi=soft -march=armv7-a -mtune=cortex-a9 $(PROCESSOR) -Wall -Werror -std=c99 -fdata-sections -ffunction-sections -I$(HWLIBS_ROOT)/include -I$(HWLIBS_ROOT)/include/socal

ifeq ($(USE_FPGA_ENABLE_DMA_SUPPORT),1)
CFLAGS += -DALT_FPGA_ENABLE_DMA_SUPPORT=1
endif

CURR_PROJECT=$(ROKAR_PRJ_NAME)
CFLAGS_cyc5soc = -DPRJ_CYC5SOC
CFLAGS_brdd = -DPRJ_BRDD
CFLAGS += $(CFLAGS_$(CURR_PROJECT))

HWLIB_SRCS = $(shell find $(HWLIBS_ROOT)/src/hwmgr -name "*.c")
HWLIB_OBJS = $(patsubst %.c,%.o,$(HWLIB_SRCS))

HWLIB = libhwlib.a

.PHONY: all
all: $(HWLIB_OBJS) $(HWLIB)

.PHONY:
clean:
	$(RM) $(HWLIB_OBJS) *.a

$(HWLIBS_ROOT)/src/hwmgr/%.o: $(HWLIBS_ROOT)/src/hwmgr/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(HWLIB):
	$(AR) rcs $@ $(HWLIB_OBJS)
